// The standard library
use <std>;
// The file file.ma from the library/folder io
use <std:io:file.ma>;
// The file ma.ma from the current folder or if not found searched in the included then standard folders
use <ma.ma>;

/**
 * Entrypoint
 * The program will look for a main method to start up
 * Optional arguments are argc and argv
 */
met main(argc::int, argv::string[])::int {

	// readonly variables cannot be modified once initialized
	readonly let variable::int = 20;

	return #5fff + variable ** 4;
}

// defaults to void
met a() {

}

// exposed a method/variable/class... to be used outside of this file
expose main;

class A {
	// The constructor of the class A. Accepts arguments
	A(){}

	// prv methods in classes are not callable outside of the class nor in instances
	prv met a() {}
	// pub methods are callable in instances
	pub met b() {}
}

// new A() creates a new instance of the class A
readonly b::A = new A();
