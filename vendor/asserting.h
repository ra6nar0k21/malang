//
// Created by Ra6nar0k21 on 09.02.22.
//

#ifndef MALANG_VENDOR_ASSERTING_H_
#define MALANG_VENDOR_ASSERTING_H_

#include <iostream>

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

#define assert_not_reached(msg) ASSERT(false, msg);


#endif //MALANG_VENDOR_ASSERTING_H_
