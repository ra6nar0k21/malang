//
// Created by Ra6nar0k21 on 11.02.22.
//

#ifndef MALANG_SRC_AST_PROGRAM_STMT_H_
#define MALANG_SRC_AST_PROGRAM_STMT_H_

#include <string>
#include <vector>
#include <parser/token.h>

#include "ast_node.h"

class ProgramStmt : public Stmt {
public:
  ProgramStmt(const std::string &programName) : _programName(programName) {}

  void accept(Visitor &visitor) override {}

  void addStatement(const std::shared_ptr<Stmt> &statement) {
    this->_statements.emplace_back(statement);
  }
private:
  std::vector<std::shared_ptr<Stmt>> _statements;
  std::string _programName;
};

#endif //MALANG_SRC_AST_PROGRAM_STMT_H_
