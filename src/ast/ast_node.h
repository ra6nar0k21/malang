//
// Created by Ra6nar0k21 on 10.02.22.
//

#ifndef MALANG_SRC_PARSER_AST_AST_NODE_H_
#define MALANG_SRC_PARSER_AST_AST_NODE_H_

enum ASTNodeType {
  eSTATEMENT,
  eEXPRESSION
};

class Visitor;

class ASTNode {
public:
  friend Visitor;

  virtual void accept(Visitor &visitor) = 0;
  virtual ASTNodeType nodeType() const = 0;
};

class Stmt : public ASTNode {
public:
  ASTNodeType nodeType() const final {
    return ASTNodeType::eSTATEMENT;
  }
};

class Expr : public ASTNode {
public:
  ASTNodeType nodeType() const final {
    return ASTNodeType::eEXPRESSION;
  }
};

#endif //MALANG_SRC_PARSER_AST_AST_NODE_H_
