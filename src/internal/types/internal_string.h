#pragma clang diagnostic push
#pragma ide diagnostic ignored "google-explicit-constructor"

//
// Created by Ra6nar0k21 on 11.02.22.
//

#ifndef MALANG_SRC_INTERNAL_TYPES_INTERNAL_STRING_H_
#define MALANG_SRC_INTERNAL_TYPES_INTERNAL_STRING_H_

#include "value.h"

class InternalString : public InternalValue {
public:
  InternalString();
  InternalString(std::string initialValue);

  std::any getRaw() override;
  int32_t asNumber() override;
  std::string asString() override;
  ValueType getType() const override;

private:
  std::string _stringValue;
};

#endif //MALANG_SRC_INTERNAL_TYPES_INTERNAL_STRING_H_

#pragma clang diagnostic pop
