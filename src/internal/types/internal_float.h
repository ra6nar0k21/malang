//
// Created by Ra6nar0k21 on 11.02.22.
//

#ifndef MALANG_SRC_INTERNAL_TYPES_INTERNAL_FLOAT_H_
#define MALANG_SRC_INTERNAL_TYPES_INTERNAL_FLOAT_H_

#include "value.h"

class InternalFloat : public InternalValue {
public:
  InternalFloat();
  /*
   * Can be explicitly assigned to uint32_t
   */
  InternalFloat(float initialValue);

  std::any getRaw() override;
  int32_t asNumber() override;
  std::string asString() override;
  ValueType getType() const override;

private:
  float _float;
};

#endif //MALANG_SRC_INTERNAL_TYPES_INTERNAL_FLOAT_H_
