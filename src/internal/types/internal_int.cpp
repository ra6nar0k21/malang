//
// Created by Ra6nar0k21 on 11.02.22.
//

#include "internal_int.h"

#include <asserting.h>

InternalInt::InternalInt() : _int() {}
InternalInt::InternalInt(int32_t initialValue) : _int(initialValue) {}

std::any InternalInt::getRaw() { return this->_int; }

int32_t InternalInt::asNumber() { return this->_int; }

std::string InternalInt::asString() { return std::to_string(this->_int); }
ValueType InternalInt::getType() const { return ValueType::eINT; }

