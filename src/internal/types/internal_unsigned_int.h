//
// Created by Ra6nar0k21 on 11.02.22.
//

#ifndef MALANG_SRC_INTERNAL_TYPES_INTERNAL_UNSIGNED_INT_H_
#define MALANG_SRC_INTERNAL_TYPES_INTERNAL_UNSIGNED_INT_H_


#include "value.h"

class InternalUnsignedInt : public InternalValue {
public:
  InternalUnsignedInt();
  InternalUnsignedInt(uint32_t initialValue);

  std::any getRaw() override;
  int32_t asNumber() override;
  std::string asString() override;
  ValueType getType() const override;

private:
  uint32_t _uint;
};

#endif //MALANG_SRC_INTERNAL_TYPES_INTERNAL_UNSIGNED_INT_H_
