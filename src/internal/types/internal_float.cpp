//
// Created by Ra6nar0k21 on 11.02.22.
//

#include "internal_float.h"

#include <asserting.h>

InternalFloat::InternalFloat() : _float() {}
InternalFloat::InternalFloat(float initialValue) : _float(initialValue) {}

std::any InternalFloat::getRaw() { return this->_float; }

int32_t InternalFloat::asNumber() { return (int32_t) this->_float; }

std::string InternalFloat::asString() { return std::to_string(this->_float); }
ValueType InternalFloat::getType() const { return ValueType::eFLOAT; }
