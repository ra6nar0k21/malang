//
// Created by Ra6nar0k21 on 09.02.22.
//

#ifndef MALANG_SRC_PARSER_LEXER_H
#define MALANG_SRC_PARSER_LEXER_H

#include "token.h"
#include <string>
#include <vector>

class Lexer {
public:
  explicit Lexer(const std::string& programPath);

  std::vector<Token> tokenize();

private:
  void clearBuffer();

  void tokenizeOperator();
  void tokenizeComment(bool multiline);
  void tokenizeIdentifier();

  void tokenizeNumber();
  void tokenizeHexNumber();
  void tokenizeBinNumber();
  void tokenizeOctNumber();

  static bool isIdentifierStart(char current);
  static bool isIdentifierPart(char current);

  char next();
  char peek(int offset) const;

  void addToken(TokenType type);
  void addToken(TokenType type, const std::string &text);
private:
  std::string _strBuf;

  std::string _fileContent;
  uint32_t _fileLength{0};

  std::vector<Token> _tokens;
  uint32_t _pos{0};
  uint32_t _row{1}, _column{1};
};

#endif//MALANG_SRC_PARSER_LEXER_H
