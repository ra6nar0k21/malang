//
// Created by Ra6nar0k21 on 09.02.22.
//

#ifndef MALANG_SRC_PARSER_TOKEN_H
#define MALANG_SRC_PARSER_TOKEN_H

// TODO: addStatement all tokens

#include <string>
#include <cstdint>
#include "asserting.h"

enum TokenType {
  // 0..9
  eNUMBER,
  // 0x0..f
  eNUMBER_HEX,
  // 0b0..1
  eNUMBER_BIN,
  // 00..8
  eNUMBER_OCT,
  eTEXT,

  eIDENTIFIER,

  // public
  ePUBLIC,
  // private
  ePRIVATE,

  // readonly
  eREADONLY,
  // let
  eLET,

  // MET
  eMET,
  // if
  eIF,
  // else
  eELSE,
  // while
  eWHILE,
  // for
  eFOR,
  // do
  eDO,
  // break
  eBREAK,
  // continue
  eCONTINUE,
  // return
  eRETURN,

  // use
  eUSE,

  // match
  eMATCH,
  // switch
  eSWITCH,
  // case
  eCASE,
  // default
  eDEFAULT,

  // class
  eCLASS,
  // interface
  eINTERFACE,
  // abstract
  eABSTRACT,

  // new
  eNEW,
  // expose
  eEXPOSE,

  // +
  ePLUS,
  // -
  eMINUS,
  // *
  eSTAR,
  // /
  eSLASH,
  // %
  ePERCENT,
  // @
  eAT,

  // =
  eEQ,
  // ==
  eEQEQ,
  // !
  eEXCL,
  // !=
  eEXCLEQ,
  // <=
  eLTEQ,
  // >=
  eGTEQ,
  // <
  eLT,
  // >
  eGT,

  // +=
  ePLUSEQ,
  // -=
  eMINUSEQ,
  // *=
  eSTAREQ,
  // /=
  eSLASHEQ,
  // %=
  ePERCENTEQ,
  // &=
  eAMPEQ,
  // ^=
  eCARETEQ,
  // |=
  eBAREQ,
  // <<=
  eLTLTEQ,
  // >>=
  eGTGTEQ,
  // >>>=
  eGTGTGTEQ,

  // ++
  ePLUSPLUS,
  // --
  eMINUSMINUS,

  // <<
  eLTLT,
  // >>
  eGTGT,
  // >>>
  eGTGTGT,

  // **
  eSTARTSAR,
  // ?:
  eQUESTIONCOLON,
  // ??
  eQUESTIONQUESTION,

  // ~
  eTILDE,
  // ^
  eCARET,
  // |
  eBAR,
  // ||
  eBARBAR,
  // &
  eAMP,
  // &&
  eAMPAMP,

  // ?
  eQUESTION,
  // :
  eCOLON,
  // ::
  eCOLONCOLON,
  // ;
  eSEMICOLON,

  // (
  eLPAREN,
  // )
  eRPAREN,
  // [
  eLBRACKET,
  // ]
  eRBRACKET,
  // {
  eLBRACE,
  // }
  eRBRACE,
  // ,
  eCOMMA,
  // .
  eDOT,

  eEOF
};

class Token {
public:
  explicit Token(TokenType type, std::string text, uint32_t row, uint32_t column);

  TokenType getType() const {
    return this->_type;
  }

  const std::string &getText() const {
    return this->_text;
  }

  uint32_t getRow() const {
    return this->_row;
  }

  uint32_t getColumn() const {
    return this->_column;
  }

  std::string toString() const {
    std::string str("<");

    str += "Type: ";
    str += std::to_string(this->_type);

    str += " InternalValue: \"";
    str += this->_text;

    str += "\" Row:Column ";
    str += std::to_string(this->_row) + ":" + std::to_string(this->_column);

    str += ">";
    return str;
  }

private:
  TokenType _type;
  std::string _text;
  uint32_t _row, _column;
};

static std::string tokenToString(TokenType token) {
  switch (token) {
    case eNUMBER: return "eNUMBER";
    case eNUMBER_HEX: return "eNUMBER_HEX";
    case eNUMBER_BIN: return "eNUMBER_BIN";
    case eNUMBER_OCT: return "eNUMBER_OCT";
    case eTEXT: return "eTEXT";
    case eIDENTIFIER: return "eIDENTIFIER";
    case ePUBLIC: return "ePUBLIC";
    case ePRIVATE: return "ePRIVATE";
    case eREADONLY: return "eREADONLY";
    case eLET: return "eLET";
    case eMET: return "eMET";
    case eIF: return "eIF";
    case eELSE: return "eELSE";
    case eWHILE: return "eWHILE";
    case eFOR: return "eFOR";
    case eDO: return "eDO";
    case eBREAK: return "eBREAK";
    case eCONTINUE: return "eCONTINUE";
    case eRETURN: return "eRETURN";
    case eUSE: return "eUSE";
    case eMATCH: return "eMATCH";
    case eSWITCH: return "eSWITCH";
    case eCASE: return "eCASE";
    case eDEFAULT: return "eDEFAULT";
    case eCLASS: return "eCLASS";
    case eINTERFACE: return "eINTERFACE";
    case eABSTRACT: return "eABSTRACT";
    case eNEW: return "eNEW";
    case eEXPOSE: return "eEXPOSE";
    case ePLUS: return "ePLUS";
    case eMINUS: return "eMINUS";
    case eSTAR: return "eSTAR";
    case eSLASH: return "eSLASH";
    case ePERCENT: return "ePERCENT";
    case eAT: return "eAT";
    case eEQ: return "eEQ";
    case eEQEQ: return "eEQEQ";
    case eEXCL: return "eEXCL";
    case eEXCLEQ: return "eEXCLEQ";
    case eLTEQ: return "eLTEQ";
    case eGTEQ: return "eGTEQ";
    case eLT: return "eLT";
    case eGT: return "eGT";
    case ePLUSEQ: return "ePLUSEQ";
    case eMINUSEQ: return "eMINUSEQ";
    case eSTAREQ: return "eSTAREQ";
    case eSLASHEQ: return "eSLASHEQ";
    case ePERCENTEQ: return "ePERCENTEQ";
    case eAMPEQ: return "eAMPEQ";
    case eCARETEQ: return "eCARETEQ";
    case eBAREQ: return "eBAREQ";
    case eLTLTEQ: return "eLTLTEQ";
    case eGTGTEQ: return "eGTGTEQ";
    case eGTGTGTEQ: return "eGTGTGTEQ";
    case ePLUSPLUS: return "ePLUSPLUS";
    case eMINUSMINUS: return "eMINUSMINUS";
    case eLTLT: return "eLTLT";
    case eGTGT: return "eGTGT";
    case eGTGTGT: return "eGTGTGT";
    case eSTARTSAR: return "eSTARTSAR";
    case eQUESTIONCOLON: return "eQUESTIONCOLON";
    case eQUESTIONQUESTION: return "eQUESTIONQUESTION";
    case eTILDE: return "eTILDE";
    case eCARET: return "eCARET";
    case eBAR: return "eBAR";
    case eBARBAR: return "eBARBAR";
    case eAMP: return "eAMP";
    case eAMPAMP: return "eAMPAMP";
    case eQUESTION: return "eQUESTION";
    case eCOLON: return "eCOLON";
    case eCOLONCOLON: return "eCOLONCOLON";
    case eSEMICOLON: return "eSEMICOLON";
    case eLPAREN: return "eLPAREN";
    case eRPAREN: return "eRPAREN";
    case eLBRACKET: return "eLBRACKET";
    case eRBRACKET: return "eRBRACKET";
    case eLBRACE: return "eLBRACE";
    case eRBRACE: return "eRBRACE";
    case eCOMMA: return "eCOMMA";
    case eDOT: return "eDOT";
    case eEOF: return "eEOF";

    default:assert_not_reached("Unknown Token: " << token);
  }
}

#endif//MALANG_SRC_PARSER_TOKEN_H
