//
// Created by Ra6nar0k21 on 11.02.22.
//

#include "ast_parser.h"
#include "asserting.h"

#include <ast/program_stmt.h>
#include <ast/use_stmt.h>
#include <ast/block_stmt.h>

static Token TokenEOF(TokenType::eEOF, "", -1, -1);

ASTParser::ASTParser(const std::vector<Token> &tokens) : _tokens(tokens), _size(tokens.size()), _position(0) {}

Token ASTParser::consume(TokenType type) {
  auto current = this->get(0);
  if (type != current.getType()) {
    assert_not_reached("Token does not match. Expected " << tokenToString(type) << " but got " << tokenToString(current.getType()));
  }
  this->_position++;
  return current;
}

bool ASTParser::matches(TokenType type) {
  auto current = this->get(0);
  if (type != current.getType()) {
    return false;
  }
  this->_position++;
  return true;
}

bool ASTParser::peekMatches(TokenType type, uint32_t offset) {
  return this->get(offset).getType() == type;
}

Token ASTParser::get(uint32_t offset) {
  auto position = this->_position + offset;
  if (position >= this->_size) {
    return TokenEOF;
  }
  return this->_tokens[position];
}

std::shared_ptr<ProgramStmt> ASTParser::parseProgram(const std::string &fileName) {
  auto program = std::make_shared<ProgramStmt>(fileName);

  // File only contains statements
  while (!this->matches(TokenType::eEOF)) {
    program->addStatement(this->parseStmt());
  }

  return program;
}

std::shared_ptr<Stmt> ASTParser::parseStmt() {
  if (this->matches(TokenType::eUSE)) {
    return this->parseUseStmt();
  }
  assert_not_reached("Invalid Statement: " << this->get(0).getType());
}

std::shared_ptr<UseStmt> ASTParser::parseUseStmt() {
  // use <expr>;
  this->consume(TokenType::eLT);

  //a:b:c
  //a:b:c.ma

  bool usesFolder = true;

  // Use can be a path delimited by :
  while (!this->matches(TokenType::eGT)) {
    auto current = this->get(0);

    if (current.getType() == TokenType::eIDENTIFIER) {

    }

    if (current.getType() == TokenType::eDOT) {

    } else if (current.getType() == TokenType::eCOLON) {

    }
  }

  auto expr = this->parseExpr();
  this->consume(TokenType::eGT);
  this->consume(TokenType::eSEMICOLON);
  return std::make_shared<UseStmt>(expr);
}

std::shared_ptr<Expr> ASTParser::parseExpr() {
  return {};
}
